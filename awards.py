#
# Copyright (c) 2020 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

# A simple utility for exploring SBIR awards.
#
# All parameters are determined via
#
#  https://www.sbir.gov/api/awards
#  https://www.sbir.gov/api/firms
#  https://www.sbir.gov/api/solicitations
#
# as retrieved on January 12, 2020.
#
# It appears that the JSON API limits its results to 100 awards, so we must be
# careful to spider the awards using sufficiently specific queries.
#
# The current approach is to download the full JSON list of 62k or so firms
# if a file named 'firms.json' is not detected and to subsequently load said
# JSON object from file and query awards for each firm.
#
import glob
import json
import os.path
import urllib.parse
import urllib.request

# The base URL for requesting JSON payloads of award metadata.
BASE_AWARD_JSON_URL = 'https://www.sbir.gov/api/awards.json'

# The base URL for requesting JSON payloads of firm metadata.
BASE_FIRM_JSON_URL = 'https://www.sbir.gov/api/firm.json'

# The base URL for requesting JSON payloads of solicitation metadata.
BASE_SOLICITATION_JSON_URL = 'https://www.sbir.gov/api/solicitations.json'

# A list of admissible agency names.
AGENCIES = {
    'DHS': 'Department of Homeland Security',
    'DOC': 'Department of Commerce',
    'DOD': 'Department of Defense',
    'DOE': 'Department of Energy',
    'DOT': 'Department of Transportation',
    'ED': 'Department of Education',
    'EPA': 'Environmental Protection Agency',
    'HHS': 'Department of Health and Human Services',
    'NASA': 'National Aeronautics and Space Administration',
    'NSF': 'National Science Foundation',
    'USDA': 'United States Department of Agriculture',
}

# A list of admissible/known award program specifications.
# TODO(Jack Poulson): Fill this through scraping instead.
PROGRAMS = {
    'SBIR': 'Small Business Innovation Research grant',
}


def firm_json_url(duns=None, firm=None, keyword=None):
    '''
    A utility for generating a JSON query URL for the firms API.

    Args:
      duns: A possible Dun and Bradstreet Number (DUNS) to limit to.
      firm: A possible company to limit results to. If non-empty, it should be
          a member of FIRMS.
      keyword: A possible keyword string to filter results using.
   
    Returns:
      URL string of the requested query.
    '''
    arguments = ''

    if duns:
        if arguments:
            arguments += '&'
        arguments += 'duns={}'.format(duns)

    if firm:
        if arguments:
            arguments += '&'
        arguments += 'firm=\'{}\''.format(urllib.parse.quote(firm))

    if keyword:
        if arguments:
            arguments += '&'
        arguments += 'keyword={}'.format(keyword)

    if arguments:
        return '{}?{}'.format(BASE_FIRM_JSON_URL, arguments)
    else:
        return BASE_FIRM_JSON_URL


def dump_firms(filename):
    '''
    A utility for retrieving and saving a JSON file of the firms.

    Args:
      filename: The filename to open.
    '''
    firm_url_string = firm_json_url()
    firm_url = urllib.request.urlopen(firm_url_string)
    firms = json.loads(firm_url.read())
    with open(filename, 'w') as json_file:
        json.dump(firms, json_file, indent=1)


def award_json_url(agency=None,
                   firm=None,
                   keyword=None,
                   program=None,
                   research_institution=None,
                   year=None):
    '''
    A utility for generating a JSON query URL for the awards API.

    Args:
      agency: A possible program agency to limit results to. If non-empty, it
          should be a member of AGENCIES.
      firm: A possible company to limit results to. If non-empty, it should be
          a member of FIRMS.
      keyword: A possible keyword string to filter results using.
      program: A possible program type to limit results to. If non-empty, it
          should be a member of PROGRAMS.
      research_institution: A possible research institution to limit search
          results to.
      year: A possible year to limit award results to.

    Returns:
      The URL string which, when accessed, returns a JSON object containing the
      first 100 awards matching the query.
    '''
    arguments = ''

    if agency:
        if arguments:
            arguments += '&'
        arguments += 'agency={}'.format(agency)

    if firm:
        if arguments:
            arguments += '&'
        arguments += 'firm=\'{}\''.format(urllib.parse.quote(firm))

    if keyword:
        if arguments:
            arguments += '&'
        arguments += 'keyword={}'.format(keyword)

    if program:
        if arguments:
            arguments += '&'
        arguments += 'program={}'.format(program)

    if research_institution:
        if arguments:
            arguments += '&'
        arguments += 'ri={}'.format(research_institution)

    if year:
        if arguments:
            arguments += '&'
        arguments += 'year={}'.format(year)

    if arguments:
        return '{}?{}'.format(BASE_AWARD_JSON_URL, arguments)
    else:
        return BASE_AWARD_JSON_URL


# TODO(Jack Poulson): Automatically sort into a separate folder for each
# agency.
def download_awards_using_firms(firms,
                                firm_award_dir='firm_awards',
                                reuse_cached_firm_awards=True):
    '''
    Retrieve list of awards from each firm and write out JSON file.

    Args:
      firms:
      firm_award_dir:
      reuse_cached_firm_awards:

    Returns:
      Dictionary mapping failed firm titles to their URL strings.
    '''
    if not os.path.exists(firm_award_dir):
        os.mkdir(firm_award_dir)
    failed_downloads = {}
    for firm in firms:
        title = firm['title']

        # Some firms have a '/' in their name, which confuses the
        # filesystem. We therefore replace any forward slashes with a dash.
        sanitized_title = title.replace('/', '-')
        filename = '{}/{}.json'.format(firm_award_dir, sanitized_title)

        if reuse_cached_firm_awards and os.path.exists(filename):
            # We have a hit for this firm.
            continue

        url_string = award_json_url(firm=title)
        print('url_string: {}'.format(url_string))
        try:
            url = urllib.request.urlopen(url_string)
            awards = json.loads(url.read())
            if 'ERROR' in awards:
                # No awards were found for this firm.
                continue
            print('Found {} awards for \'{}\''.format(len(awards), title))

            with open(filename, 'w') as json_file:
                json.dump(awards, json_file, indent=1)
        except:
            print('Could not download awards for \'{}\''.format(title))
            failed_downloads[title] = url_string

        print('')

    if failed_downloads:
        print('Attempting cleanup for failed_downloads={}'.format(
            failed_downloads))
        still_failed_downloads = {}
        for title in failed_downloads:
            # Some firms have a '/' in their name, which confuses the
            # filesystem. We therefore replace any forward slashes with a dash.
            sanitized_title = title.replace('/', '-')
            filename = '{}/{}.json'.format(firm_award_dir, sanitized_title)
            url_string = failed_downloads[title]
            print('url_string: {}'.format(url_string))
            try:
                url = urllib.request.urlopen(url_string)
                awards = json.loads(url.read())
                if 'ERROR' in awards:
                    # No awards were found for this firm.
                    continue
                print('Found {} awards for \'{}\''.format(len(awards), title))
                with open(filename, 'w') as json_file:
                    json.dump(awards, json_file, indent=1)
            except:
                print('Could not download awards for \'{}\''.format(title))
            still_failed_downloads[title] = url_string

    if still_failed_downloads:
        print('Could still not download still_failed_downloads={}'.format(
            still_failed_downloads))

    return still_failed_downloads


def solicitation_json_url(agency=None,
                          only_open=False,
                          only_closed=False,
                          keyword=None):
    '''
    A utility for generating a JSON query URL for the solicitations API.

    Args:
      agency: A possible agency to limit the results to.
      only_open: If specified, only open solicitations will be returned.
      only_closed: If specified, only closed solicitations will be returned.
      keyword: A possible keyword string to filter results using.

    Returns:
      URL string of the requested query.
    '''
    arguments = ''

    if agency:
        if arguments:
            arguments += '&'
        arguments += 'agency={}'.format(agency)

    if only_open:
        if arguments:
            arguments += '&'
        arguments += 'open=1'
    elif only_closed:
        if arguments:
            arguments += '&'
        arguments += 'closed=1'

    if keyword:
        if arguments:
            arguments += '&'
        arguments += 'keyword={}'.format(keyword)

    if arguments:
        return '{}?{}'.format(BASE_SOLICITATION_JSON_URL, arguments)
    else:
        return BASE_SOLICITATION_JSON_URL


def is_link(input_str):
    '''
    Attempts to detect whether or not a string is an HTTP or HTTPS link.

    Args:
      input_str: The input string to classify.

    Returns: 
      Whether the string is believed to be a link or not.
    '''
    if len(input_str) <= 7:
        # The string is too short to contain 'http://' or 'https://'.
        return False

    if input_str[:7] is 'http://' or input_str[:8] is 'https://':
        return True
    else:
        return False


def normalize_string(input_str):
    '''
    Returns the normalized analogue of a given string.

    Args:
      input_str: The input string.

    Returns:
      The normalization of the string.
    '''
    if is_link(input_str):
        return input_str.lower()
    else:
        # Strip out a certain list of characters.
        CHARS_TO_STRIP = '\'[”#$%’()*+-/<=>@[\]^_`{|}~]'
        stripped_str = input_str.translate(
            str.maketrans('', '', CHARS_TO_STRIP))

        # Remove non-ASCII characters from the string.
        stripped_str = ''.join(i for i in stripped_str if ord(i) < 128)

        # Lowercase the string.
        normalized_str = stripped_str.lower()

        return normalized_str


# TODO(Jack Poulson): Avoid the quadratic complexity in the depth of the
# nested dictionary.
def normalize_json(json_object):
    '''
    Normalizes the leaves in a JSON object.

    Args:
      dictionary: The input JSON object.

    Returns:
      The normalized JSON object.
    '''
    normalized_json = None
    if isinstance(json_object, tuple) or isinstance(json_object, list):
        normalized_json = []
        for item in json_object:
            normalized_json.append(normalize_json(item))
    elif isinstance(json_object, dict):
        normalized_json = {}
        for key in json_object:
            normalized_json[key] = normalize_json(json_object[key])
    elif isinstance(json_object, str):
        normalized_json = normalize_string(json_object)
    else:
        # This case should never happen, but we handle it nevertheless.
        normalized_json = json_object

    return normalized_json


def sample_agency_awards():
    '''
    Prints a list of (first 100) titles of awards from each agency in 2018-2019.
    '''
    years = ['2018', '2019']
    for year in years:
        for agency in AGENCIES:
            url_string = award_json_url(agency=agency, year=year)
            url = urllib.request.urlopen(url_string)
            awards = json.loads(url.read())
            if 'ERROR' in awards:
                print(
                    'Encountered error in retrieving {} awards for {}'.format(
                        year, agency))
                continue
            print('{}, {} titles [{}]:'.format(agency, year, len(awards)))
            for award in awards:
                print('  {}'.format(award['title']))
            print('')


def sample_agency_solicitations():
    '''
    Prints a list of (first 100) titles of solicitations from each agency.
    '''
    for agency in AGENCIES:
        url_string = solicitation_json_url(agency=agency, only_open=True)
        url = urllib.request.urlopen(url_string)
        solicitations = json.loads(url.read())
        if 'ERROR' in solicitations:
            print(
                'Error in retrieving open solicitations for {}'.format(agency))
            continue
        print('{} open titles [{}]:'.format(agency, len(solicitations)))
        for solicitation in solicitations:
            print('  {} {}'.format(solicitation['SolicitationTitle'],
                                   solicitation['SBIRSolicitationLink']))
        print('')

    for agency in AGENCIES:
        url_string = solicitation_json_url(agency=agency, only_closed=True)
        url = urllib.request.urlopen(url_string)
        solicitations = json.loads(url.read())
        if 'ERROR' in solicitations:
            print('Error in retrieving closed solicitations for {}'.format(
                agency))
            continue
        print('{} closed titles [{}]:'.format(agency, len(solicitations)))
        for solicitation in solicitations:
            print('  {} {}'.format(solicitation['SolicitationTitle'],
                                   solicitation['SBIRSolicitationLink']))
        print('')


def normalize_directory(raw_directory, normalized_directory):
    '''
    Creates a normalizes analogue of a directory of JSON files.

    Args:
      raw_directory: The directory containing the unnormalized JSON objects.
      normalized_directory: The directory to write the normalized JSON to.
    '''
    num_dir_chars = len(raw_directory) + 1
    unnormalized_files = glob.glob('{}/*.json'.format(raw_directory))
    if not os.path.exists(normalized_directory):
        os.mkdir(normalized_directory)
    for unnormalized_file in unnormalized_files:
        name = unnormalized_file[num_dir_chars:]
        normalized_file = '{}/{}'.format(normalized_directory, name)
        print('Converting {} to {}'.format(unnormalized_file, normalized_file))

        with open(unnormalized_file) as input_file:
            unnormalized_json = json.load(input_file)
            normalized_json = normalize_json(unnormalized_json)
        with open(normalized_file, 'w') as output_file:
            json.dump(normalized_json, output_file, indent=1)


def main():
    FIRMS_PATH = 'firms.json'
    sample_awards = True
    sample_solicitations = True

    if os.path.exists(FIRMS_PATH):
        print('File \'{}\' exists.'.format(FIRMS_PATH))
    else:
        # The request takes a few seconds and I don't want to hammer the server.
        print('Did not find \'{}\', so creating...'.format(FIRMS_PATH))
        dump_firms(FIRMS_PATH)
    with open(FIRMS_PATH) as firms_json_file:
        firms = json.load(firms_json_file)
    print('Loaded {} firms'.format(len(firms)))
    download_awards_using_firms(firms)

    if sample_awards:
        sample_agency_awards()

    if sample_solicitations:
        sample_agency_solicitations()


if __name__ == '__main__':
    main()
